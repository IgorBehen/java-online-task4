package recording.song_manager;

import recording.music_types.MusicStyle;
import recording.music_types.Song;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class SongManager {
    public static List<Song> song = new ArrayList<Song>();
    public static Scanner sc = new Scanner(System.in);

    public static void BurnTheCollectionToDisk() {
        String styleIn = null;
        int countOfBurnedSongs = 0;
        System.out.println("To sort by style, enter a style name(CLASSIC, ROCK, JAZZ, FOLK, POP): ");
        try {
            styleIn = sc.nextLine().toUpperCase().trim();
            MusicStyle styleEn = MusicStyle.valueOf(styleIn);
            for (Song s : song) {
                if (s.getMusicStyle() == styleEn) {
                    System.out.println("sortingByStyle == style: " + s.toString());
                    countOfBurnedSongs++;
                }
            }

        } catch (IllegalArgumentException e) {
            System.out.println("This is a wrong character");
        }
        System.out.println("The collection of songs is recorded on a disc!" + " " + countOfBurnedSongs + " "
               + styleIn + " style songs have been recorded.");
        System.out.println();
    }

    public void CalculateTheDurationOfTheCollection() {
        double durationSongsInCollection = 0;
        for (Song music : song) {
            durationSongsInCollection += music.getSongDuration();
        }
        System.out.println("Duration songs in the collection: " + durationSongsInCollection + " sec.");
        System.out.println();
    }

    public void SearchSongByDuration() {
        System.out.println("Enter a min and max song duration: ");
        try {
            Double minDuration = sc.nextDouble();
            Double maxDuration = sc.nextDouble();
            for (int i = 0; i < song.size(); i++) {
                if (song.get(i).getSongDuration() >= minDuration && song.get(i).getSongDuration() <= maxDuration) {
                    System.out.println("This is your song: " + song.get(i).toString());
                }
            }
        } catch (InputMismatchException e) {
            System.out.println("This is a wrong character");
        }
        System.out.println();
    }

    public void sortingByStyle() {
        System.out.println("To sort by style, enter a style name(CLASSIC, ROCK, JAZZ, FOLK, POP): ");
        try {
            String styleIn = sc.nextLine().toUpperCase().trim();
            MusicStyle styleEn = MusicStyle.valueOf(styleIn);
            for (Song s : song) {
                if (s.getMusicStyle() == styleEn) {
                    System.out.println("sortingByStyle == style: " + s.toString());
                }
            }

        } catch (IllegalArgumentException e) {
            System.out.println("This is a wrong character");
        }
        System.out.println();
    }

    public static void initData() {
        // songs
        song.add(new Song(1, "Eine kleine Nachtmusik", "Mozart",
                18.54, MusicStyle.CLASSIC));
        song.add(new Song(2, "Toccata and Fugue in D minor", "J.S. Bach",
                9.32, MusicStyle.CLASSIC));
        song.add(new Song(3, "STAIRWAY TO HEAVEN", "LED ZEPPELIN",
                8.03, MusicStyle.ROCK));
        song.add(new Song(4, "Hotel California", "Eagles",
                6.31, MusicStyle.ROCK));
        song.add(new Song(5, "Take Five", "Dave Brubeck",
                5.29, MusicStyle.JAZZ));
        song.add(new Song(6, "So What", "Miles Davis",
                9.23, MusicStyle.JAZZ));
        song.add(new Song(7, "This Land Is Your Land", "Woody Guthrie",
                4.13, MusicStyle.FOLK));
        song.add(new Song(8, "Blowin' in the Wind", "Bob Dylan",
                2.47, MusicStyle.FOLK));
        song.add(new Song(9, "Don't Speak", "No Doubt",
                5.03, MusicStyle.POP));
        song.add(new Song(10, "The Sign", "Ace Of Base",
                3.18, MusicStyle.POP));

        for (Song music : song) {
            System.out.println(music);
        }
        System.out.println();
    }
}
