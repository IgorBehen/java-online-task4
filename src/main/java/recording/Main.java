package recording;

import recording.menu.Menu;
import recording.song_manager.SongManager;

/**
 * OOP TASK (with classes, inheritance, polymorphism, encapsulation)
 * На свій вибір вибрати один з варіантів задачі:
 * https://learn.by/courses/course-
 * v1:EPAM+JColl+ext1/courseware/2c1004eb30e64bae9b37ff47e5f23406/297eb2
 * 62310a4daa9d37a6ec7825bed2/1
 * Також для цієї задачі створіть консольне меню, використовуючи ENUM.
 */

public class Main {
    public static void main(String[] args) {
        SongManager sm = new SongManager();
        sm.initData();
        Menu.menu();
    }
}
