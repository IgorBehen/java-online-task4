package recording.music_types;

public enum MusicStyle {
    CLASSIC, ROCK, JAZZ, FOLK, POP;
}
