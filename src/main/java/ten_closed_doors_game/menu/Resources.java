package ten_closed_doors_game.menu;

public class Resources {

    public static void Help() {
        System.out.println("☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣");
        System.out.println("    Герой, що володіє силою в 25 балів, знаходиться в круглому\n" +
                "залі, з якого ведуть 10 закритих дверей. За кожними дверима героя чекає або\n" +
                "магічний артефакт, що дарує силу від 10 до 80 балів, або монстр, який має\n" +
                "силу від 5 до 100 балів, з яким герою потрібно битися. Битву виграє персонаж,\n" +
                "що володіє найбільшою силою; якщо сили рівні, перемагає герой.\n" +
                "    Для того, щоб обрати двері натисніть цифри від 1 до 10.");
        System.out.println("☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣☣\n");
    }

    public static void About(){
        System.out.println("");
        System.out.println("");
        System.out.println("❁❁❁   ＭＡＤＥ  ＢＹ  ＩＨＯＲ  ＢＥＨＥＮ   ❁❁❁  ");
        System.out.println("");
        System.out.println("                  ✦２０１９✦                     ");
        System.out.println("");
        System.out.println("                  \uD835\uDD3C ℕ \uD835\uDD41 \uD835\uDD46 \uD835\uDD50!   ");
    }

    public static void startPic() {
        System.out.println("░▀█░█████████████████▀▀░░░██░████");
        System.out.println("▄▄█████████████████▀░░░░░░██░████");
        System.out.println("███▀▀████████████▀░░░░░░░▄█░░████");
        System.out.println("███▄░░░░▀▀█████▀░▄▀▄░░░░▄█░░▄████");
        System.out.println("░███▄▄░░▄▀▄░▀███▄▀▀░░▄▄▀█▀░░█████");
        System.out.println("▄▄█▄▀█▄▄░▀▀████████▀███░░▄░██████");
        System.out.println("▀████▄▀▀▀██▀▀██▀▀██░░▀█░░█▄█████░");
        System.out.println("░░▀▀███▄░▀█░░▀█░░░▀░█░░░▄██████░▄");
        System.out.println("████▄▄▀██▄░█░░▄░░█▄░███░████▀▀░▄█");
        System.out.println("█▀▀▀▀▀▀░█████▄█▄▄████████▀░▄▄░▄██");
        System.out.println();
    }

    public static void lose() {
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("                             .-═^\"`          `\"\"^═.");
        System.out.println("                        ═\"                            \"─");
        System.out.println("                   ⌐^                                      '~");
        System.out.println("                ⌐`¡                                            ═");
        System.out.println("             ¿`    █,                                       █    `¬");
        System.out.println("           ⌐     ƒ █▄█           ╙▄        ▄              ▄▐▌ ▌     >");
        System.out.println("         /       █▄██▐█             *▄▄▄▄█▀              ▄Q██▄▌       \\");
        System.out.println("       ¿        ╒█   ▀██▄▄▄,          ▀█▀           ,▄▄██▌▀  ▀█         <");
        System.out.println("'               ▐█         `▀,         ║         ¿▀        ,  █▌         '");
        System.out.println("     `           ██ ███▄      '        ║               ▄███  █▀            ");
        System.out.println("   ,             █ ▐▐████▄  ▄                     ▄▄A▄████▌█ ▐▌             ,");
        System.out.println("                 █ ▌██████████▄     ▄ ,   ▄▄,▄  ,██████████▐  █              ");
        System.out.println("                 ' ▐█████████████▄▄███▌ ██████▄████████████▌▌ `                ");
        System.out.println(" ƒ                 ████████████████▀ █▌ ▐█ ╘████████████████▐                 \\");
        System.out.println("                   █▌█████████████▀  █   █  ▀██████████████\\▀                   ");
        System.out.println("╒                 ╙C      ,▄▄█▀▀   █,█▄█▄█   ▀▀█▄▄,,   ▀▀`                      [");
        System.out.println("[                $                 ▐▌█████▌▌                  ▌                   ]");
        System.out.println("h                ▐                 ▌███████▐                 ,▌                   ");
        System.out.println("U                 █▄▄φ,,,,,       ]▐███████▌▌     ,▄▄█▀▀▀,,▄██                    ");
        System.out.println("L                    ▀████∞▄▀███   ▐███████▌  , j█▀▀▄∞████▀-                     j");
        System.out.println("├                      ▌▄███▄, ▐▀  ▐██▄█▄██▌█▌   U ▄▄██\"▐                      ⌠");
        System.out.println("                       ▐█████▐Ω⌐  ]▀▀▀ ' ▀█▀▀█    ▄████▌▌                      C");
        System.out.println("L                       ██████▐▌  '             ] █████▌                      ╒");
        System.out.println("                        ▐██████,    ,          ╓ █▀████                        ");
        System.out.println("╘                        ▀▀███▀█▄▌  ▌        █,▐▌▐,██▀                       ╛");
        System.out.println("  \\                       █▌█j▌▐▀▀█▄∞∞▐╓∞▄█▀▀- ▌▐▌██                       /");
        System.out.println("    )                      ▐█▌▀▌▐Ç ▐   █   ║ ] ▄█`██▌                      (");
        System.out.println("     '                      ╙▌█▌ █═█▄  █  ,█▄█▀`██▀▀                      ╛");
        System.out.println("       ╕                     \"\"▌▀█  ▌- █▀`▐▌ ▐█▀█║                      ¿");
        System.out.println("        ',                     ┘ █-▀█▄▄█▄▄██'▐ ▐                      ,'");
        System.out.println("          ',                     ╟  ▐  █  ▐  ▓                      ,'");
        System.out.println("            '~                      ▐  █  █                       .\"");
        System.out.println("                ,                      ▓  '                    ,²");
        System.out.println("                   .                   '                    .^");
        System.out.println("");
        System.out.println("");
        System.out.println("");
    }

    public static void win() {
        System.out.println("                   ,▄▄╦@@Ñ▀▀▓▓▓╣ÑÑß@╦╥,");
		System.out.println("               ╓╦╣╢█▀░ ░░░░░░░░░░░░░░░░╙╢▓╦,");
		System.out.println("            ╥╣▒╢╜░╙░░░░░░░░░░╥▒████▄▄▒▒▒▒▒▒▒╣▓w");
		System.out.println("         ╓╣▒▒╣╜░░░╓▄▄███@░░░╠▓▀` ` `└▀█▒▒▒▒▒▒▒▒▒@,");
		System.out.println("       ╓▓▒╣╢▒░░░░▄█▀`   ╙╫▒▒▒▒∩ ╓╦w,   ╙▓▒▒▒▒▒╢▒╢▒▓,");
		System.out.println("     ,╬▒╣╣╣░░░░▒▓█░ ,,,    ▓▒▒▌ ▌█,╓N   '▓▒▒▒▒▒▒▒▒▒▒▓");
		System.out.println("    ╓╣╣▒╣╣░▒░▒▒╠█░ ▐▒▄  ,   ▓▒▒N╙███▒▐  ,╢╢▒▒╢▒▒▒▒╢╢╣╣┐");
		System.out.println("   ╓▒▒▒╢╣▒▒▒▒▒▒║█░ ▐▐███▒W  └▒▒▒▒▄▀╣▒▓ .▒╢╣╢╢╢▒▒▒▒▒▒▒╢▒╕");
		System.out.println("   ╣╢▒▒▒▒▒▒▒▒▒▒▒▒▓  ╫▀▀█▒▌   ▌▒▒▒▒▒@▄,░j▒╢╣╣╣▒▒▒▒╢╢▒╣▒╣╫");
		System.out.println("  ╫╣▒▒▒▒▒▒▒▒▒▒▒▒▒▒╢╢╣▒▒▓▀  ,▓▒▒▒▒▒▒▒▒▒▒▒▓▒╢▒▒▒@▒W@╣╢╢▒▒╢▓");
		System.out.println("  ╣▒▒▒▒▒▒▒▒▒▒▒▒╣╢@╢╢╢╣╣▒▓@╣╜▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▒▒▒▒▒▒▒▒╣╫");
		System.out.println("  ╣╣▒▒▒▒▒▒▒▒▒▒╢╢╢╢╢╣╢╢╣╢▒▒▒▒░░░▒▒▒▒▒▒▒▒▒▒╫╣█▓▒▒▒▒▒▒▒▒▒▒╣╫");
		System.out.println("  ╣╣▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒╢╣╬╝╜░░█▓▒▒▒▒▒▒▒▒▒▒▒╢╫");
		System.out.println("  ╢▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒███▒╙╙╜╙╜╙╙╙╙'    ╓██▓▒▒▒╣▒▓▓▒▒╫▒╢╢▓               gÑ▒N,");
		System.out.println("  ]╣╢▒▒╣▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓███▄▄▄,,,,,▄▄▄█████▒▒▓▒▓▒▒▒▒▒╫▓╣╢C               ▒▒▒▒▒W");
		System.out.println("   ▓╣▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█████████████████▓▒▓▒▓▒▒▒╣▒▒▒▒╟▓         Æ▒░▒▒L╓▒▒▒▒▓▒");
		System.out.println("    ▓╣╢▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀▓▓▒▒▀▓▓▒▒▓███▓▒▒▓▓▒▒▒▓▒▒▒▒▓▓          ▒▒▒▒▒▓╬▒▒▒▒▒▓C");
		System.out.println("     ╚▒▒╢╣▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒╬▓▓▓▓▓▓▓▓▒▒▒▒▓▒▒▒▒▒▒▒░Ñ▒▓          ╢▒▒▐░▒▒▒▒▒▒╢╫");
		System.out.println("      \"╬▒▒▒▒╢▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒╢╣▒▒▒▒╢╢╢╢▒▒▒▓▒▒▒▒▒▒▒▓▒▒╫         ╓▒▒▒▒▒░▒▒▒▒▒╣▓");
		System.out.println("        ╙╬@▒▒▒▒▒╢▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▒▒▒▒▒▒▒╙▒▓        ╣▒▒▒▒▒▒▒▓▒▒▒▒▓");
		System.out.println("           ╨╣@░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒╢╢▓▒▒▒▒▒▒▒░k       ▓╫╜░░░▒▒▒▒▒▒▒▓");
		System.out.println("             `╨╣@▒▒▒▒▒▒╢▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒╢▒╬╜` ╣▒▒▒▒▒▒▒░W     ▓▒▒▒▒▒▒▒▒▒▒▓╜");
		System.out.println("                 `╙╩Ñ@▒▒▒▒▒▒▒▒▒▒▒▒▒╢▒▒╬╩╜`      \"╫▒▒▒▒▒▒╟     ╟▒╣╣╣▒╢╣▒▓\"");
		System.out.println("");
		System.out.println("");
		System.out.println("");

    }

}
