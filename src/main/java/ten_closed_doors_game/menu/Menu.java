package ten_closed_doors_game.menu;

import ten_closed_doors_game.GameOnArrays;

import java.util.Scanner;

public enum Menu {
    FINISH,
    START,
    HELP,
    CHANCE,
    ABOUT;

    static Scanner scanner;

    public static void startmenu() {
        System.out.println();
        System.out.println("    ❃❃❃.❃❃❃.❃❃❃.ー \uD835\uDD44 \uD835\uDD3C ℕ \uD835\uDD4C ー.❃❃❃.❃❃❃.❃❃❃             ");
        System.out.println("❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃");
        System.out.println("1. ✓ To start the game, enter                            ★ - 1");
        System.out.println("2. ✓ For Help, enter                                     ★ - 2");
        System.out.println("3. ✓ Chances of choosing a door, enter                   ★ - 3");
        System.out.println("4. ✓ About, enter                                        ★ - 4");
        System.out.println("0. ✓ To finish the game, press                           ★ - 0");
        System.out.println("❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃");
        System.out.println("             ❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃❃             ");
        System.out.println();
        menu();
    }

    static int getEnteredNumber(int n) {
        try {
            scanner = new Scanner(System.in);
            n = (Integer) scanner.nextInt();
        } catch (Exception e) {
            System.out.println("Incorrect input! Try again!222");
        }
        return n;
    }

    public static void menu() {
        int x = 0;
        int n = -1;
        Menu number = null;

        while (n != 0) {
            n = getEnteredNumber(n);
            x = n;
            if (x >= 0 && x <= 4) {
                number = Menu.values()[x];
            } else {
                System.out.println("Enter the number correctly! From 1 to 10!");
                startmenu();
            }
            if (number != null) {
                switch (number) {
                    case FINISH:
                        System.out.println("Goodbye!");
                        System.exit(0);
                        break;
                    case START:
                        GameOnArrays.startGame();
                        break;
                    case HELP:
                        Resources.Help();
                        break;
                    case CHANCE:
                        GameOnArrays.doorsRandom();
                        GameOnArrays.chancesTable();
                        break;
                    case ABOUT:
                        Resources.About();
                        break;
                    default:
                        throw new IllegalArgumentException();
                }
            }
        }
    }
}