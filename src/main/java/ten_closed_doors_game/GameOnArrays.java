package ten_closed_doors_game;

import ten_closed_doors_game.menu.Menu;
import ten_closed_doors_game.menu.Resources;

import java.util.Scanner;

public class GameOnArrays {
    static int[] doors = new int[10];
    static int hero;
    static int numberOfDoors = 0;
    static int doorNumber;
    static Scanner scanner;
    static int doorValue;

    public static void doorsRandom() {
        for (int i = 0; i < 10; i++) {
            int prize = -100 + (int) (Math.random() * ((80 - (-100)) + 1));
            if (prize <= 80 && prize >= -100) {
                if (prize < 10 && prize > -5) {
                    prize += 15;
                    doors[i] = prize;
                } else {
                    doors[i] = prize;
                }
            }
        }
    }

    public static void choiceOfDoor() {
        if (doorValue > 0) {
            System.out.println();
            System.out.println(" ☺ You have found a magical artifact with " + doorValue +
                    " points of health ❤. Now your health is " + hero + " points.");
        } else {
            System.out.println();
            System.out.println("☹ You met a monster that damaged you by " + doorValue +
                    " points of health ☠. Now your health is " + hero + " points.");
        }

    }

    static int getEnteredNumber(int k) {
        try {
            scanner = new Scanner(System.in);
            k = (Integer) scanner.nextInt();
        } catch (Exception e) {
            System.out.println("Incorrect input! Try again!222");
        }
        return k;
    }

    public static void startGame() {
        doorsRandom();
        hero = 25;
        int n = 0;
        while (numberOfDoors < 10 && hero > 0 && hero != 1000) {
            System.out.println();
            System.out.println("==============================================");
            System.out.println("➤ Enter the number of the door from 1 to 10: ");
            System.out.println("↩ Press 11 to return to the previous menu.");
            System.out.println("==============================================");
            doorNumber = getEnteredNumber(n);
            if (doorNumber <= 11 && doorNumber >= 0) {
                if (doorNumber == 11) {
                    Menu.startmenu();
                    hero = 1000;
                }
                for (int i = 0; i < doors.length; i++) {
                    if (i == doorNumber - 1) {
                        doorValue = doors[i];
                        switch (i) {
                            case 0:
                                hero += doorValue;
                                doorsRandom();
                                choiceOfDoor();
                                break;
                            case 1:
                                hero += doorValue;
                                doorsRandom();
                                choiceOfDoor();
                                break;
                            case 2:
                                hero += doorValue;
                                doorsRandom();
                                choiceOfDoor();
                                break;
                            case 3:
                                hero += doorValue;
                                doorsRandom();
                                choiceOfDoor();
                                break;
                            case 4:
                                hero += doorValue;
                                doorsRandom();
                                choiceOfDoor();
                                break;
                            case 5:
                                hero += doorValue;
                                doorsRandom();
                                choiceOfDoor();
                                break;
                            case 6:
                                hero += doorValue;
                                doorsRandom();
                                choiceOfDoor();
                                break;
                            case 7:
                                hero += doorValue;
                                doorsRandom();
                                choiceOfDoor();
                                break;
                            case 8:
                                hero += doorValue;
                                doorsRandom();
                                choiceOfDoor();
                                break;
                            case 9:
                                hero += doorValue;
                                doorsRandom();
                                choiceOfDoor();
                                break;
                            default:
                                System.out.println("Something went wrong! :( Select a door from 1 to 10.");
                        }
                        numberOfDoors++;
                        System.out.println("✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧✧");
                        System.out.println(" ✪ You have already opened " + numberOfDoors + " doors. You still have "
                                + (10 - numberOfDoors) + " doors left to open.");
                    }
                }

            }
        }
        if (hero != 1000) {
            if (hero >= 0) {
                numberOfDoors = 0;
                System.out.println();
                System.out.println();
                System.out.println("\uD835\uDCE8\uD835\uDCDE\uD835\uDCE4 \uD835\uDCE6\uD835\uDCD8\uD835\uDCDD！！！ ㋡㋡㋡");
                System.out.println();
                System.out.println();
                Resources.win();
                Menu.startmenu();
            } else {
                numberOfDoors = 0;
                System.out.println();
                System.out.println();
                System.out.println("\uD835\uDCE8\uD835\uDCDE\uD835\uDCE4 \uD835\uDCDB \uD835\uDCDE \uD835\uDCE2 \uD835\uDCD4！！！ ☠☠☠");
                System.out.println();

                Resources.lose();

                Menu.startmenu();
            }
        }
    }

    static int countPositive = 0;
    static int countNegative = 0;

    public static int chancesTableСount(int i, int[] doors) {
        if (i == (doors.length - 1)) {
            return doors[i];
        }
        if (doors[i] > 0) {
            countPositive++;
        } else {
            countNegative++;
        }
        return countNegative + countPositive + chancesTableСount(i + 1, doors);
    }

    public static void chancesTable() {
        String leftAlignFormat = "| %-10s | %-8d |%n";
        System.out.println("Chances of choosing a door:");
        System.out.println("⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻⁻");
        System.out.format("+------------------------+----------+%n");
        System.out.format("| Number of door values  | Quantity |%n");
        System.out.format("+------------------------+----------+%n");
        chancesTableСount(0, doors);
        System.out.format(leftAlignFormat, " With monsters        ", countNegative);
        System.out.format(leftAlignFormat, " With prizes          ", countPositive);
        System.out.format("+------------------------+----------+%n");
        countPositive = 0;
        countNegative = 0;
    }
}