package container_for_strings_array;

import java.util.ArrayList;
import java.util.Random;

/**
 * Create a container that encapsulates an array of String, and that only adds
 * Strings and gets Strings, so that there are no casting issues during use. If the
 * internal array isn’t big enough for the next add, your container should
 * automatically resize it. In main( ), compare the performance of your container
 * with an ArrayList holding Strings.
 */

public class MainContainerForStrings {

    private static String generateString() {
        Random ran = new Random();
        int top = 5;
        char data = ' ';
        String dat = "";

        for (int i = 0; i <= top; i++) {
            data = (char) (ran.nextInt(25) + 97);
            dat = data + dat;
        }
        //System.out.println(dat);
        return dat;
    }

    public static String string = MainContainerForStrings.generateString();

    public static long addTimeContainerForStrings(ContainerForStrings stringCont, int reps) {
        long start = System.nanoTime();
        for (int i = 0; i < reps; i++) {
            stringCont.add(string);
        }
        long stop = System.nanoTime();
        return (stop - start) / reps;
    }

    public static long addTimeArrayList(ArrayList<String> arrl, int reps) {
        long start = System.nanoTime();
        for (int i = 0; i < reps; i++) {
            arrl.add(string);
        }
        long stop = System.nanoTime();
        return (stop - start) / reps;
    }

    public static long getTimeArrayList(ArrayList<String> arrl, int reps) {
        long start = System.nanoTime();
        for (int i = 0; i < reps; i++) {
            arrl.get(i);
        }
        long stop = System.nanoTime();
        return (stop - start) / reps;
    }

    public static long getTimeContainerForStrings(ContainerForStrings stringCont, int reps) {
        long start = System.nanoTime();
        for (int i = 0; i < reps; i++) {
            stringCont.get(i);
        }
        long stop = System.nanoTime();
        return (stop - start) / reps;
    }

    public static void main(String[] args) {
        ArrayList<String> arrList1 = new ArrayList<String>();
        ContainerForStrings containerForStrings1 = new ContainerForStrings();
        System.out.println("ArrayList add time (reps: 100):            " + addTimeArrayList(arrList1, 100));
        System.out.println("ContainerForStrings add  time (reps: 100): " + addTimeContainerForStrings(containerForStrings1, 100));
        System.out.println("ArrayList get time (reps: 100):            " + getTimeArrayList(arrList1, 100));
        System.out.println("ContainerForStrings get time (reps: 100):  " + getTimeContainerForStrings(containerForStrings1, 100));
        System.out.println();
        ArrayList<String> arrList2 = new ArrayList<String>();
        ContainerForStrings containerForStrings2 = new ContainerForStrings();
        System.out.println("ArrayList add time (reps: 1000):            " + addTimeArrayList(arrList2, 1000));
        System.out.println("ContainerForStrings add  time (reps: 1000): " + addTimeContainerForStrings(containerForStrings2, 1000));
        System.out.println("ArrayListget time (reps: 1000):             " + getTimeArrayList(arrList2, 1000));
        System.out.println("ContainerForStrings get time (reps: 1000):  " + getTimeContainerForStrings(containerForStrings2, 1000));
    }
}
