package container_for_strings_array;

public class ContainerForStrings {
    private int size = 0;
    private int index = 0;
    private String[] arr = new String[size];

    public void add(String s) {
        if (index < arr.length) {
            arr[index++] = s;
        } else {
            size++;
            String[] newArr = new String[size];
            for (int i = 0; i < arr.length; i++)
                newArr[i] = arr[i];
            newArr[index++] = s;
            arr = newArr;
        }
    }

    public String get(int i) {
        if (i > -1 && i < arr.length)
            return arr[i];
        throw new ArrayIndexOutOfBoundsException(i);
    }
}
