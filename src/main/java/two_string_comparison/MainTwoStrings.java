package two_string_comparison;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * Create a class containing two String objects and make it Comparable so that the
 * comparison only cares about the first String. Fill an array and an ArrayList with
 * objects of your class by using a custom generator (eg, which generates pairs of
 * Country-Capital). Demonstrate that sorting works properly. Now make a
 * Comparator that only cares about the second String and demonstrate that
 * sorting works properly. Also perform a binary search using your Comparator.
 */

public class MainTwoStrings implements Comparable<MainTwoStrings> {
    private String first = "";
    private String second = "";

    public MainTwoStrings(String str1, String str2) {
        first = str1;
        second = str2;
    }

    // Compare by first string
    public int compareTo(MainTwoStrings mts) {
        return first.compareTo(mts.first);
    }

    // Compare by second string
    public static class StringsCompomparator implements Comparator<MainTwoStrings> {
        public int compare(MainTwoStrings mts1, MainTwoStrings mts2) {
            return mts1.second.compareTo(mts2.second);
        }
    }

    public String toString() {
        return first + ", " + second;
    }

    static void printArray(MainTwoStrings[] msa) {
        System.out.print("[");
        for (int i = 0; i < msa.length - 1; i++)
            System.out.print(msa[i] + ", ");
        System.out.println(msa[msa.length - 1] + "]");
    }

    private static String generateString() {
        Random ran = new Random();
        int top = 5;
        char data = ' ';
        String dat = "";
        for (int i = 0; i <= top; i++) {
            data = (char) (ran.nextInt(25) + 97);
            dat = data + dat;
        }
        return dat;
    }

    public static void main(String[] args) {
        String rand = MainTwoStrings.generateString();
        String rand2 = MainTwoStrings.generateString();
        MainTwoStrings[] array = new MainTwoStrings[5];
        List<MainTwoStrings> list = new ArrayList<MainTwoStrings>();
        for (int i = 0; i < array.length; i++) {
            String s1 = rand;
            String s2 = rand2;
            array[i] = new MainTwoStrings(s1, s2);
            list.add(new MainTwoStrings(s1, s2));
        }
        System.out.print("Array: ");
        printArray(array);
        System.out.println("List:  " + list);
        Arrays.sort(array);
        Collections.sort(list, null);
        System.out.println();
        System.out.println("Sorted by first word:");
        System.out.print("Array: ");
        printArray(array);
        System.out.println("List:  " + list);
        MainTwoStrings.StringsCompomparator compar = new MainTwoStrings.StringsCompomparator();
        Arrays.sort(array, compar);
        Collections.sort(list, compar);
        System.out.println();
        System.out.println("Sorted by second word:");
        System.out.print("Array: ");
        printArray(array);
        System.out.println("List:  " + list);
    }
}
