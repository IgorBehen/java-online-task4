package delete_more_than_two;

/**
 * Видалити в масиві всі числа, які повторюються більше двох разів.
 */

public class MainDelMoreThanTwo {
    static int removeDuplicates(int arr[], int n) {
        if (n == 0 || n == 1) {
            return n;
        }
        int j = 0;
        for (int i = 0; i < n - 2; i++) {
            if (arr[i] == arr[i + 1] && arr[i] == arr[i + 2]) {
                System.out.print("");
            } else if (arr[i] == arr[i + 1]) {
                arr[j++] = arr[i];
            } else if (arr[i] != arr[i + 1] && arr[i] != arr[i + 2]) {
                arr[j++] = arr[i];
            }
        }
        arr[j++] = arr[n - 2];
        arr[j++] = arr[n - 1];
        return j;
    }

    public static void main(String[] args) {
        int arr[] = {1, 2, 2, 2, 2, 3, 4, 4, 4, 5, 5, 5, 5, 6};
        int n = arr.length;
        n = removeDuplicates(arr, n);
        for (int i = 0; i < n; i++)
            System.out.print(arr[i] + " ");

    }
}
