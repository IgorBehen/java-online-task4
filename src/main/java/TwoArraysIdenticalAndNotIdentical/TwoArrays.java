package TwoArraysIdenticalAndNotIdentical;

/**
 * Дано два масиви. Сформувати третій масив, що складається з тих елементів,
 *  які: а) присутні в обох масивах; б) присутні тільки в одному з масивів.
 */

public class TwoArrays {

    public static void main(String[] args) {
        int[] arrA = {1, 2, 3, 4, 5};
        int[] arrB = {5, 4, 3, 6, 7};
        int[] arrIdentic;
        int[] arrNotIdentic;
        int identic = 0;
        int notIdentic = 0;
        boolean check;

        for (int i = 0; i < arrA.length; i++) {
            for (int j = 0; j < arrB.length; j++) {
                if (arrA[i] == arrB[j]) {
                    identic++;
                }
            }
        }

        arrIdentic = new int[identic];

        for (int i = 0; i < arrA.length; i++) {
            for (int j = 0; j < arrB.length; j++) {
                if (arrA[i] == arrB[j]) {
                    arrIdentic[j] = arrA[i];
                }
            }
        }
        arrNotIdentic = new int[arrA.length-identic];

        for (int i = 0; i < arrA.length; i++) {
            check = true;
            for (int j = 0; j < arrB.length; j++) {
                if (arrA[i] == arrB[j]) {
                    check = false;
                    notIdentic--;
                }
            }
            if(check)
                arrNotIdentic[notIdentic] = arrA[i];
            notIdentic++;
        }

        for (int k = 0; k < arrIdentic.length; k++)
            System.out.print(arrIdentic[k] + " ");

        System.out.println();
        
        for (int i=0; i< arrNotIdentic.length; i++)
            System.out.print(arrNotIdentic[i]+" ");
    }
}
